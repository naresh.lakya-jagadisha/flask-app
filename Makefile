PORT ?= 5000

.PHONY: build run

build:
	docker build -t flask-app .

run:
	docker container run -it -p $(PORT):5000 flask-app

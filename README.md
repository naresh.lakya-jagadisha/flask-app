# Flask-App

Contains example code to containerize python flask application.

## Usage

Building the docker image.

```bash
make build
```

Running the docker image.

```bash
make run
```

Application can be accessed using http://localhost:5000

To run the app on different port. For instance, port 8080.

```bash
PORT=8080 make run
```

FROM python:3.9.12-slim

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

ENV FLASK_APP=hello
CMD [ "python", "-m", "flask", "run", "--host=0.0.0.0"]
